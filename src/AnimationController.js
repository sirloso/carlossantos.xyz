// animate the barcode getting bigger
export const onMouseEnter = (renderer, camera,scene, glassCubes) => {
    // let em = document.createElement('div')
    // em.id = 'enter'
    // em.innerHTML = 'click to enter'
    // em.style.fontFamily = 'Gruppo'
    // em.style.fontSize = '2em'
    // em.style.marginLeft = '2.5em'
    // let _this = document.getElementById('threebarcode')
    // // _this.style.width = `${window.innerWidth}px`
    // _this.style.height = `${window.innerHeight}px`
    // _this.style.background = 'white'
    // _this.style.flexDirection = 'column'
    // setTimeout(() => {
    //     if(!document.getElementById('enter')) _this.insertBefore(em, _this.children[0])
    //     resizeCanvas(renderer, camera,scene,glassCubes)
    // }, 1000)
}

// animate returning to normal 
export const onMouseLeave = (renderer, camera, removeGlass) => {
    return
    let _this = document.getElementById('threebarcode')
    _this.style.position = ''
    _this.style.width = '20em'
    _this.style.height = '3em'
    _this.style.background = ''
    _this.removeChild(_this.children[0])
    let canvas = renderer.domElement
    renderer.setSize(window.screen.innerWidth,window.screen.innerHeight)
    canvas.style.width = '20em'
    canvas.style.height = '3em'
    camera.updateProjectionMatrix();
    removeGlass()
}

// transition into works page
export const onDoubleClick = (threeController) => {
    // window.location.href = 'works'
    // let _this = document.getElementById('threebarcode')
    // _this.removeChild(_this.children[0])

    // threeController.scene.remove(
    //     threeController.controller.codeGroup
    // )
    // _this.style.position = `absolute`
    // _this.style.left = 0
    // _this.style.top = 0
    // _this.style.width = `${window.screen.width/2}px`
    // _this.style.height = `${window.screen.height/2}px`
    // _this.style.background = 'white'
    // _this.style.flexDirection = 'column'
    // // threeController.scene.background = new TB.THREE.Color('red')
    // threeController.renderer.setSize(window.screen.width, window.screen.height)
}

export function getPx() {
    // var div = document.getElementById('d');
    // div.style.height = '1em';
    // return div.offsetHeight
}

function resizeCanvas(renderer, camera,scene,glassCubes) {
    const canvas = renderer.domElement;
    if(!canvas) return
    // look up the size the canvas is being displayed
    let container = canvas.parentElement
    if(!container) return
    const widthPx = container.style.width
    const heightPx = container.style.height
    const width = container.style.width.split('px')[0]
    const height = container.style.height.split('px')[0]

    // adjust displayBuffer size to match
    if (canvas.width !== width || canvas.height !== height) {
        console.log('resize', width, height);
        renderer.setSize(width, height, false);
        canvas.style.width = widthPx
        canvas.style.height =heightPx 

        glassCubes[0].cube.position.x = 1.5
        glassCubes[0].cube.position.z = 10
        glassCubes[0].cube.startingPosition = 'right'
        glassCubes[1].cube.position.x = -0.5
        // glassCubes[1].cube.position.z = 1.7
        glassCubes[1].cube.startingPosition = 'left'
        if(window.screen.innerWidth<800)camera.aspect = 1
            camera.updateProjectionMatrix();
        if(glassCubes[0]) glassCubes[0].plane.scale.set(width,height)

        for(let i = 0;i<glassCubes.length;++i){
            let gc = glassCubes[i]
            scene.add(gc.cube)
        }
    }
}
