import React, { Component } from 'react';
const source = require("./lines.json")['data']

const last = (index) => {return index===source.length-2 };
const translate = (index) => {return index=== 6 || index===14};
const untranslated = (index) => {return index===10 }
const translated = (index) => {return index === 12}
const constants = (index) => {return index===8 || index===16}
const start = (index) => {return index<6}

class SourceContainer extends Component {
  render() {
      return (
        <div>
        <div className="ref">running source code:</div>
        {source.map((string,index)=>{
            if(
              string!=='' &&
              start(index)
          ){
              return(
                <pre key={index} className="start">
                  <code>
                    {string}
                  </code>
                </pre>
              )
            }else if(last(index) ||constants(index)){
              return(
                <pre key={index} className="constant">
                  <code>
                    {string}
                  </code>
                </pre>
              )
            }else if(translate(index)){
              if(this.props.h) return( <pre key={index} className="translate"><code>{string}</code></pre> )
              else return(<pre key={index} ><code>{string}</code></pre>)
            }else if(untranslated(index)){
              return(
                <span key={index}>
                {
                  !this.props.translated &&(<pre key={index} className="constant"><code>{string}</code></pre>)
                }
                {
                  this.props.translated &&(<pre key={index} ><code>{string}</code></pre>)
                }
                </span>
              );
            }else if(translated(index)){
              return(
                <span key={index}>
                {
                  this.props.translated &&(<pre key={index} className="constant"><code>{string}</code></pre>)
                }
                {
                  !this.props.translated &&(<pre key={index} ><code>{string}</code></pre>)
                }
                </span>
              );
            }else{
              return("")
            }
            })
        }
        </div>
      );
  }
}

export default SourceContainer;
