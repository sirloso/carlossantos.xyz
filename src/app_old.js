class App extends Component {
  constructor(props) {
    super(props);
    let { scene, camera } = TB.createScene('hello')
    scene.background = new TB.THREE.Color( 'white' )
    camera.position.x = 1.675;
    camera.position.z = 5.75;
    this.state = { width: 0, height: 0, translate: false, hovering: false, scene , camera, renderer:{render:()=>{}}};
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    this.animate = this.animate.bind(this)
  }
  componentDidMount() {
    this.updateWindowDimensions();
    let renderer = TB.createRenderer('threebarcode')
    this.setState({renderer})
    this.animate()
    window.addEventListener('resize', this.updateWindowDimensions);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }
  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  translate = () => {
    this.setState({ translate: !this.state.translate })
  }

  animate(){
    requestAnimationFrame(this.animate);
    this.state.renderer.render(this.state.scene, this.state.camera);
  }
  render() {
    return (
      <div className="App">
        <div className="Left">
          {
            !this.state.translate && (
              <div>
                <div className="hello">hello,</div>
                <div className="Untranslated">
                  Ìmy name is Carlos Santos.gÎ
                  <br />
                  Ìi am a full-stack developer.uÎ
                  <br />
                  Ìmy work can be found here:CÎ
                  <br />
                  <a href="https://gitlab.com/cs4026"> Ìhttps://gitlab.com/csÇH:gÎ </a>
                  <br />
                </div>
              </div>
            )
          }

          {
            this.state.translate && (
              <div>
                <div className="helloT">hello,</div>
                <div className="Translated">

                  my name is Carlos Santos.
                <br />
                  <br />
                  i’m a full-stack developer living in new york.
                <br />
                  <br />
                  my work can be found here:
                <br />
                  <div id='threebarcode'></div>
                  <br />
                  <a href="https://gitlab.com/cs4026">  https://gitlab.com/cs4026</a>
                  <br />
                  <br />
                </div>
              </div>
            )
          }

          <div className="TranslateButton" onMouseEnter={() => {
            this.setState({ hovering: true })
          }} onMouseLeave={() => {
            this.setState({ hovering: false })
          }} onClick={this.translate}>
            _click_to_translate
        </div>

          <span id="email"><a style={{ color: "black", textDecoration: "none" }} id="pdf" href="http://bit.ly/2sx3WNm">los@carlossantos.xyz</a></span>
        </div>

        <div className="Right">
          <div className="Contain">
            <SourceContainer translated={this.state.translate} h={this.state.hovering} />
          </div>
        </div>

      </div>
    );
  }
}