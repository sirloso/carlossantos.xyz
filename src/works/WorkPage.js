import React from 'react'
import AwesomeSlider from 'react-awesome-slider';

const WorkPage = (props) => {
    let work = props.work
    return (
        <div className="WorkPage">
            {
                work && (
                    <div>
                        <div id="work_title" onClick={() => { console.log('sdfsdfsdf') }}>
                            {work.title}
                        </div>
                        <AwesomeSlider className='monkey' bullets={false}>
                            {
                                work.photos.map((ph, idx2) => {
                                    return (
                                        <div key={idx2} id={`photo_${idx2}`}>{ph}</div>
                                    )
                                })
                            }
                        </AwesomeSlider>
                        <br />
                        <div id='works_summary'>
                            {work.summary}
                        </div>
                    </div>
                )
            }
        </div>
    )
}

export default WorkPage