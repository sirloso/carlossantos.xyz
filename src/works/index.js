import WorkMenuPage from './WorkMenuPage'
import WorkPage from './WorkPage'

module.exports={
    WorkMenuPage,
    WorkPage
}