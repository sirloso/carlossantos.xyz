import React, { useState } from 'react'
import WorkPage from './WorkPage';

const WorkMenuPage = (props) => {
    const [currentWork, updateCurrentWork] = useState(false)
    const [none, updateNone] = useState(true)
    none = none
    const updateWork = (work) => {
        updateNone(false)
        updateCurrentWork(work)
    }

    let works = props.works
    return (
        <div className="WorksMenuPage">
            <div className="WorksMenu">
                {
                    works.map((section, i) => {
                        return (
                            <div className="section" key={i} id={`section_${i}`}>
                                <span id="section_title">
                                    {section.sectionTitle}
                                </span>
                                <div id="works">
                                    {
                                        section.works.map((work, idx) => {
                                            let t = work.title
                                            let ct = currentWork !== null ? currentWork.title : 'unreacbaasdfsf'
                                            let wt = t === ct ? 'underline' : ''
                                            return (
                                                <div className="work" key={idx} id={`work_${idx}`}>
                                                    <span id="title" onTouchEnd={() => { updateWork(work) }} onClick={() => { updateWork(work) }}>
                                                        <span id={wt}>
                                                            {work.title}
                                                        </span>
                                                    </span>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        )
                    })
                }
            </div>
            <WorkPage uw={updateWork} work={currentWork}></WorkPage>
        </div>
    )
}

export default WorkMenuPage