carlossantos.xyz

Inspired By Bradford W. Paley's - Code Profiles.

Site made with React.

Install and Run:<br />
  git clone https://gitlab.com/cs4026/carlossantos.xyz.git  <br />
  cd carlossantos.xyz  <br />
  npm i  <br />
  npm start  <br />

  ![website_vid.gif](./website_vid.gif)
